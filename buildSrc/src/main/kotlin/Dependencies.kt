object Versions {
  const val junit = "5.4.2"
}

object Libs {
  const val junit = "org.junit.jupiter:junit-jupiter:${Versions.junit}"
}
