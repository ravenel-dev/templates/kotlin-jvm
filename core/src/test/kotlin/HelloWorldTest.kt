import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HelloWorldTest {
  @Test
  fun testHello() {
    val hello = HelloWorld()
    assertEquals("Hello, World!", hello.hello())
  }
}
